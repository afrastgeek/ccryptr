import Vue from 'vue';
import Router from 'vue-router';
import About from '@/components/About';
import Encryptor from '@/components/Encryptor';
import Decryptor from '@/components/Decryptor';
import Welcome from '@/components/Welcome';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Welcome',
      component: Welcome,
    },
    {
      path: '/encryptor',
      name: 'Encryptor',
      component: Encryptor,
    },
    {
      path: '/decryptor',
      name: 'Decryptor',
      component: Decryptor,
    },
    {
      path: '/about',
      name: 'About',
      component: About,
    },
  ],
});
